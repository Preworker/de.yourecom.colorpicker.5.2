<?php
namespace wcf\system\option;
use wcf\data\option\Option;
use wcf\system\database\util\PreparedStatementConditionBuilder;
use wcf\system\exception\UserInputException;
use wcf\system\Regex;
use wcf\system\WCF;
use wcf\util\StringUtil;

/**
 * Option type implementation for color pickers.
 *
 * @author 	Sascha Greuel <sascha@softcreatr.de>, Marcel Beckers
 * @license	Beerware <https://de.wikipedia.org/wiki/Beerware>
 * @package	de.yourecom.colorpicker
 */
class ColorpickerOptionType extends AbstractOptionType implements ISearchableUserOption {
	/**
	 * @inheritDoc
	 */
	public function getFormElement(Option $option, $value) {
		WCF::getTPL()->assign([
			'option' => $option,
			'value' => $value,
			'allowEmptyValue' => $option->allowEmptyValue
		]);
		
		return WCF::getTPL()->fetch('optionTypeColorpicker');
	}
	
	/**
	 * @inheritDoc
	 */
	public function getSearchFormElement(Option $option, $value) {
		return $this->getFormElement($option, $value);
	}
	
	/**
	 * @inheritDoc
	 */
	public function validate(Option $option, $newValue) {
		if (empty($newValue) && !$option->allowEmptyValue) {
			throw new UserInputException($option->optionName, 'empty');
		}
		
		if (!empty($newValue)) {
			$regex = new Regex('rgba\(\d{1,3}, \d{1,3}, \d{1,3}, (1|1\.00?|0|0?\.[0-9]{1,2})\)');
			
			if (!$regex->match($newValue)) {
				throw new UserInputException($option->optionName, 'validationFailed');
			}
		}
	}
	
	/**
	 * @inheritDoc
	 */
	public function getCondition(PreparedStatementConditionBuilder &$conditions, Option $option, $value) {
		if (!isset($_POST['searchOptions'][$option->optionName])) return false;
		
		$value = StringUtil::trim($value);
		
		if ($value == '') {
			$conditions->add("option_value.userOption" . $option->optionID . " = ?", ['']);
		}
		else {
			$conditions->add("option_value.userOption" . $option->optionID . " LIKE ?", ['%' . addcslashes($value, '_%') . '%']);
		}
		
		return true;
	}
}
