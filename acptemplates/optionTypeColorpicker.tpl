{if !$colorSelectorKey|empty}
	{assign var=__colorSelectorKey value=$colorSelectorKey}
	{assign var=isColorOption value=false}
{else if $option|isset && $option|is_object && !$option->optionName|empty}
	{assign var=__colorSelectorKey value=$option->optionName}
	{assign var=isColorOption value=true}
{else}
	{assign var=__colorSelectorKey value=''}
	{assign var=isColorOption value=false}
{/if}

{if $colorTitle|isset}
	{assign var=__colorTitle value=$colorTitle}
{else}
	{assign var=__colorTitle value=''}
{/if}

{if !$colorSelectorValue|empty}
	{assign var=__colorSelectorValue value=$colorSelectorValue}
{else if !$value|empty}
	{assign var=__colorSelectorValue value=$value}
{else}
	{assign var=__colorSelectorValue value=''}
{/if}

{if !$colorSelectorAllowEmpty|empty || !$allowEmptyValue|empty}
	{assign var=__colorSelectorAllowEmpty value='1'}
{else}
	{assign var=__colorSelectorAllowEmpty value='0'}
{/if}

{if !$__colorSelectInitJS|isset}
	{assign var=__colorSelectInitJS value='1'}
	
	<script data-relocate="true" src="{@$__wcf->getPath()}js/WCF.ColorPicker.js"></script>
	<script data-relocate="true">
		$(function() {
			new WCF.ColorPicker('.optionTypeColorpicker > .colorPreview > .jsColorPicker');
			
			WCF.Language.addObject({
				'wcf.style.colorPicker': '{lang}wcf.style.colorPicker{/lang}',
				'wcf.style.colorPicker.new': '{lang}wcf.style.colorPicker.new{/lang}',
				'wcf.style.colorPicker.current': '{lang}wcf.style.colorPicker.current{/lang}',
				'wcf.style.colorPicker.button.apply': '{lang}wcf.style.colorPicker.button.apply{/lang}'
			});
			
			var removeColor = function(event) {
				var self = event.currentTarget || event.srcElement;
				var parent = self.parentNode;
				var selector = elBySel('.jsColorPicker', parent);
				
				selector.removeAttribute("style");
				elData(selector, "color", "");
				
				elAttr(elById(elData(selector, "store")), "value", "");
				
				$(elById(elData(selector, "store"))).trigger('change');
				
				self.style.display = 'none';
			};
			
			elBySelAll(".optionTypeColorpicker > .remove", document, function(elem) {
				elem.addEventListener(WCF_CLICK_EVENT, removeColor);
				
				$(elBySel('.dataStore', elem.parentNode)).change( function() {
					$elem = $(this);
					if($elem.val() != "") {
						$elem.parent().find(".remove").show();
					}
				});
			});
		});
	</script>
{/if}

<figure class="optionTypeColorpicker box64" data-input-name="{$__colorSelectorKey}">
	<div class="colorPreview">
		<div class="jsColorPicker jsTooltip" title="{$__colorTitle}" style="background-color: {$__colorSelectorValue}" data-color="{$__colorSelectorValue}" data-store="{$__colorSelectorKey}_value"></div>
	</div>
	
	{if $__colorSelectorAllowEmpty}
		<span class="remove pointer icon icon16 fa-times jsTooltip" title="{lang}wcf.global.button.delete{/lang}"{if $__colorSelectorValue|empty} style="display:none;"{/if}></span>
	{/if}
	
	<input class="dataStore" type="hidden" id="{$__colorSelectorKey}_value" name="{if $isColorOption}values[{$__colorSelectorKey}]{else}{$__colorSelectorKey}{/if}" value="{$__colorSelectorValue}" /> 
</figure>
